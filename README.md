# README #

Módulo de Tradução do Openbravo para Português Brasileiro.

### Informações Gerais ###

* OpenbravoERP 
* PR14Q2.5

### Notas de Tradução ###
# Módulos pendentes de tradução
- org.openbravo.userinterface.skin.250to300Comp
- org.openbravo.v3
- org.openbravo.v3.datasets
- org.openbravo.v3.framework

### Pacotes Adicionais á Traduzir ###
- org.openbravo.client.analytics
- org.openbravo.dataimport
- org.openbravo.utility.monitoring.appdynamics
- org.openbravo.module.pickingandpackaging
- org.openbravo.warehouse.packing
- org.openbravo.warehouse.pickinglist

 [Mais Informações](http://wiki.openbravo.com/wiki/How_to_Create_and_Update_Translation_Modules)